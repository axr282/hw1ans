% comment: you got check plus. learn to use section (%% symbol in the
% beginning of the line, I changed it for you. Then you can run code by
% section.
clear all
diary('All results')
%% Question: 1
disp('Question 1')
X= [ 1 1.5 3 4 5 7 9 10]
Y1= -2 + 0.5*X
Y2= -2 + 0.5*X.^2
figure
plot(X,Y1,X,Y2)
%% Question: 2
disp('Question 2')
X = linspace(-10,20,201)
S = sum(X)
%% Qu 3
disp('Question 3')
A = [2 4 6 ; 1 7 5; 3 12 4]
B = [-2 3 10]'
C =  A.'*B
D= (inv(A.'*A))*B
E= sum(sum((A.'*diag(B)).'))
F=A(:,1:2)
F(2,:)=[]
x=(inv(A))*B
%% Q 4
disp('Question 4')
A = [2 4 6 ; 1 7 5; 3 12 4]
B= kron(eye(5),A)
%% Q 5
disp('Question 5')
A= normrnd(10,5,5,3)
B = A>10
%% Q 6
disp('Question 6')
load('datahw1.csv');
X= datahw1(:, [3:4 6]);
Y= datahw1(:, 5);
lm = fitlm(X,Y,'linear')
diary off