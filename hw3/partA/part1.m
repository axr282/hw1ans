%first we solve for FOC. then putting theta2 = theta1/(y1), we get an
%equation just in terms of theta1. Then its simply a root-finding problem.
clear all
clc
addpath('C:\Users\abhishek\Documents\MATLAB\compecontool\CEtools')
x = gamrnd(10,5,100,1); 
disp('True value of paratemeter theta1 is:  10') 
y1 = sum(x)/100;
y2 = exp(sum(log(x))/100);
y = y1/y2;
theta1 = 3;
for it=1:10000
    [obj,grad] = objgrad(theta1,y);
    theta1 = theta1 - grad/obj;
    if norm(obj) < (1e-2)
        break
    end
end
disp('Estimate of theta1 is:')
disp(theta1)