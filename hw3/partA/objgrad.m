function [obj,grad] = objgrad(theta1,y)

obj = log(theta1/y)-psicomp(theta1);
grad = (1/theta1) - trigamma(theta1);

end