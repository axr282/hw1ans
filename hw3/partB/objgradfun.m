function [obj, grad] = objgradfun(beta,X,y);
[row,col] = size(X);
obj =  -(-ones(1,row)*exp(X*beta) + y'*X*beta - ones(1,row)*log(factorial(y)));
grad = -(sum((y-exp(X*beta))*ones(1,6).*X));
