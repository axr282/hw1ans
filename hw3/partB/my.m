clc;
clear all;
addpath('C:\Users\abhishek\Documents\MATLAB\compecontool\CEtools')
load('hw3.mat')
beta0 = [0;0;0;0;0;0];
%Fminunc without supplying the derivative 
obj = @(beta)objectfun(beta,X,y);
[betahat,fval,exitflag,output] = fminunc(obj,beta0);
disp('MLE FMINUNC method without derviative')
disp('Estimated Beta')
disp(betahat)
disp('Number of iterations')
disp(output.iterations)
disp('Number of function evaluations')
disp(output.funcCount)
%Fminunc with derivative supplied
options = optimoptions('fminunc','Algorithm','trust-region','SpecifyObjectiveGradient',true);
objgrad = @(beta)objgradfun(beta,X,y);
[betahat,value,exitflag,output] = fminunc(objgrad,beta0);
disp('FMINUNC method with derviative supplied')
disp('Estimated Beta')
disp(betahat)
disp('Number of iterations')
disp(output.iterations)
disp('Number of function evaluations')
disp(output.funcCount)
%Nelder mead
optset('neldmead','maxit',10000);
obj1 = @(beta)objfun(beta,X,y);
[betahat]=neldmead(obj1,beta0);
disp('Nelder Mead')
disp('Estimated Beta')
disp(betahat)
%BHHH method
betao= [0;0;0;0;0;0];
for i=1:500
    if i == 500
        disp('500 iterations,no convergence')
    end
    H = ((y-exp(X*betao))*ones(1,6).*X)'*((y-exp(X*betao))*ones(1,6).*X);
    J = (sum((y-exp(X*betao))*ones(1,6).*X));
    betan = betao+(J/H)'; 
    if norm(betan - betao) < 1e-6
        beta = betan;
        n = i;
    break
    end
    betao = betan;
end
disp('BHHH maximum liklihood method')
disp('Estimated Beta')
disp(betan)
disp('the number of iterations')
disp(n)
H0 = ((y-exp(X*betao))*ones(1,6).*X)'*((y-exp(X*betao))*ones(1,6).*X);
disp('the eigenvalues for inital hessian:')
disp(eig(H0))
disp('the eigenvalues for hessian approximation')
disp(eig(H))
obj = @(beta) objnls(beta,X,y);
[betahat,fval,exitflag,output] = fminunc(obj,beta0);
disp('Non-linear least squares')
disp('Estimated Beta')
disp(betahat)
disp('Number of iterations')
disp(output.iterations)
disp('Number of function evaluations')
disp(output.funcCount)