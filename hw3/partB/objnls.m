function [obj] = objnls(beta,X,y)
obj = sum((y-exp(X*beta)).^2);